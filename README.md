This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Using: React, Antd, Scss.

## Design Session

- Task 1:
  Design: SignIn and Register Page.

- Task 2:
  Design: Eod Dashboard: Current bank details and Add New Bank Details

- Task 3:
  Design: Pending Request Dasboard Page.

## API Integration and Basic Functionality In React

- Task 4:
  API Part: to get the user data. and list the users in card style, add the search functionality to filter the user from the list.
