import React from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./redux/store";

import Register from "./pages/register/register-page";
import Signin from "./pages/signin/signin-page";
import PendingRequest from "./pages/pending_request/pending-request-page";
import AddNewBankDetails from "./pages/add_new_bank_details/add_new_bank_details-page";
import CurrentBankDetails from "./pages/current_bank_details/current_bank_details-page";
import Users from "./pages/users/users-page";

const Routes = () => {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route exact path="/register" component={Register} />
          <Route exact path="/signin" component={Signin} />
          <Route
            exact
            path="/add/new/bankdetails"
            component={AddNewBankDetails}
          />
          <Route
            exact
            path="/current/bankdetails/view"
            component={CurrentBankDetails}
          />
          <Route exact path="/pendingrequest" component={PendingRequest} />
          <Route exact path="/users" component={Users} />
          <Redirect from="/" to="/register" />
        </Switch>
      </BrowserRouter>
    </Provider>
  );
};

export default Routes;
