import React from 'react'
import RegisterFunctional from './components/register-functional'

import './register.scss'

export default function Register() {
  return (
    <RegisterFunctional />
  )
}
