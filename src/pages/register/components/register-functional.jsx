import React from "react";
import RegisterPresentational from "./register-presentational";

export default function RegisterFunctional() {
  const onChange = (e) => {
    console.log(`checked = ${e.target.checked}`);
  };
  const onFinish = (values) => {
    console.log("Received values of form: ", values);
  };
  return <RegisterPresentational onFinish={onFinish} onChange={onChange} />;
}
