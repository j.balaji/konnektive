import React from "react";
import {
  Row,
  Col,
  Radio,
  Form,
  Input,
  Button,
  Typography,
  Checkbox,
} from "antd";
import { images } from "../../../assets/images";

const { Title, Text, Link } = Typography;

export default function RegisterPresentational({ onFinish, onChange }) {
  return (
    <Row className="register_container">
      <Col xl={16} className="register_image">
        <img src={images.register} alt="registrationimage" />
      </Col>
      <Col xl={8} className="register_form">
        <Col className="register_form_logo">
          <img src={images.logo_sm} alt="logo" />
        </Col>
        <Col className="register_form_title">
          <Title>
            Register <span style={{ color: "brown" }}>Here</span>
          </Title>
        </Col>
        <Col xl={24}>
          <Form name="register" onFinish={onFinish} scrollToFirstError>
            <Col xl={24} className="register_form_item_style">
              <Radio.Group name="radiogroup" defaultValue={1}>
                <Radio value={1}>Affliate</Radio>
                <Radio value={2}>Merchant</Radio>
              </Radio.Group>
            </Col>
            <Col xl={24} className="register_form_item_style">
              <Form.Item
                name="email"
                label="E-mail address"
                rules={[
                  {
                    type: "email",
                    message: "Please enter valid E-mail address!",
                  },
                  {
                    required: true,
                    message: "Please enter your E-mail address!",
                  },
                ]}
              >
                <Input placeholder="E-mail address" />
              </Form.Item>
            </Col>
            <Row>
              <Col xl={12} className="register_form_item_style">
                <Form.Item
                  name="firstName"
                  label="First Name"
                  rules={[
                    {
                      required: true,
                      message: "Please enter your First Name!",
                    },
                  ]}
                >
                  <Input placeholder="First Name" />
                </Form.Item>
              </Col>
              <Col xl={12} className="register_form_item_style">
                <Form.Item
                  name="lastName"
                  label="Last Name"
                  rules={[
                    {
                      required: true,
                      message: "Please enter your Last Name!",
                    },
                  ]}
                >
                  <Input placeholder="Last Name" />
                </Form.Item>
              </Col>
            </Row>
            <Col className="register_form_item_style">
              <Form.Item
                name="phonenumber"
                label="Phone Number"
                rules={[
                  {
                    required: true,
                    message: "Please enter your Phone Number!",
                  },
                ]}
              >
                <Input placeholder="Phone Number" />
              </Form.Item>
            </Col>
            <Col className="register_form_item_style">
              <Text>
                Tell us About Yourself:{" "}
                <span style={{ color: "brown" }}>[optional]</span>
              </Text>
            </Col>
            <Row>
              <Col xl={12} className="register_form_item_style">
                <Form.Item
                  name="businessname"
                  label="Business Name"
                  rules={[
                    {
                      required: false,
                    },
                  ]}
                >
                  <Input placeholder="Business Name" />
                </Form.Item>
              </Col>
              <Col xl={12} className="register_form_item_style">
                <Form.Item name="url" label="URL" rules={[{ required: false }]}>
                  <Input placeholder="Website URL" />
                </Form.Item>
              </Col>
            </Row>
            <Col className="register_form_item_style">
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  Request Access
                </Button>
              </Form.Item>
            </Col>
            <Col className="register_form_item_style">
              <Checkbox onChange={onChange} className="register_form_service">
                By logging in, You agree to the
                <span className="register_form_link">
                  <Link>terms of services</Link>
                </span>
              </Checkbox>
            </Col>
          </Form>
        </Col>
      </Col>
    </Row>
  );
}
