import React from "react";
import SigninFunctional from "./components/signin-functional";

import "./signin.scss";

export default function Signin() {
  return <SigninFunctional />;
}
