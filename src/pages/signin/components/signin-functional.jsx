import React from "react";
import SigninPresentational from "./signin-presentational";

export default function SigninFunctional() {
  const onChange = (e) => {
    console.log(`checked = ${e.target.checked}`);
  };
  const onFinish = (values) => {
    console.log("Received values of form: ", values);
  };
  return <SigninPresentational onFinish={onFinish} onChange={onChange} />;
}
