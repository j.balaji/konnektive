import React from "react";
import { Row, Col, Form, Input, Button, Typography, Checkbox } from "antd";
import { images } from "../../../assets/images";

const { Title, Link } = Typography;

export default function SigninPresentational({ onFinish, onChange }) {
  return (
    <Row className="signin_container">
      <Col xl={16} className="signin_image">
        <img src={images.welcome} alt="welcomeimage" />
      </Col>
      <Col xl={8} className="signin_form">
        <Col className="signin_form_logo">
          <img src={images.logo_sm} alt="logo" />
        </Col>
        <Col className="sigin_form_title">
          <Title>
            Sign <span style={{ color: "blue" }}>In</span>
          </Title>
        </Col>
        <Col xl={24}>
          <Form name="signin" onFinish={onFinish} scrollToFirstError>
            <Col xl={24} className="signin_form_item_style">
              <Form.Item
                name="email"
                label="E-mail address"
                rules={[
                  {
                    type: "email",
                    message: "Please enter valid E-mail address!",
                  },
                  {
                    required: true,
                    message: "Please enter your E-mail address!",
                  },
                ]}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col className="signin_form_item_style">
              <Form.Item
                name="password"
                label="Password"
                rules={[
                  {
                    required: true,
                    message: "Please enter password",
                  },
                ]}
              >
                <Input.Password />
              </Form.Item>
            </Col>

            <Col className="signin_form_item_style">
              <Link className="signin_form_link">Forgot Password ?</Link>
            </Col>
            <Col className="signin_form_item_style">
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  Sign In
                </Button>
              </Form.Item>
            </Col>
            <Col className="signin_form_item_style">
              <Checkbox onChange={onChange} className="signin_form_service">
                By logging in, You agree to the
                <span className="signin_form_link">
                  <Link>terms of services</Link>
                </span>
              </Checkbox>
            </Col>
          </Form>
        </Col>
      </Col>
    </Row>
  );
}
