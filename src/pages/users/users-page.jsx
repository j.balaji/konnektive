import React from "react";

import "./users.scss";
import UsersFunctional from "./components/users-functional";

export default function Users() {
  return <UsersFunctional />;
}
