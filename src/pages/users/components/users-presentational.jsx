import React from "react";
import { Row, Typography, Col, Card, Input } from "antd";

const { Search } = Input;
const { Meta } = Card;

export default function UsersPresentational({
  isLoading,
  persons,
  searchData,
  isError,
}) {
  return isLoading ? (
    <Row>
      <Typography.Title>Loading...</Typography.Title>
    </Row>
  ) : isError ? (
    <Row>
      <Typography.Text>{isError.error}</Typography.Text>
    </Row>
  ) : (
    <Row className="user_container" xl={24} sm={12} md={16}>
      <Row className="user_search">
        <Search
          placeholder="Search Here"
          onChange={searchData}
          bordered={true}
          size="large"
          style={{
            textAlign: "right",
            height: "64px",
            lineHeight: "64px",
            boxShadow: "0 1px 4px rgba(0,21,41,.12)",
            padding: "0 32px",
            width: "500px",
          }}
        />
        <br />
      </Row>
      <Row>
        <Col xl={24} className="user_header">
          <Typography.Title>List of Names</Typography.Title>
        </Col>
        <Row xl={24} className="user_cards">
          {persons.map((person, key) => (
            <Col key={key} className="user_card">
              <Card
                hoverable
                style={{ width: 300 }}
                cover={
                  <img src={person.picture.medium} alt={person.name.first} />
                }
              >
                <Meta
                  title={
                    <Typography.Title level={4}>
                      {person.name.first} {person.name.last}`
                    </Typography.Title>
                  }
                  description={
                    <Row>
                      <Typography.Text>
                        <b>Contact Details:</b>
                      </Typography.Text>
                      <Typography.Text>{person.email}</Typography.Text>
                      <Typography.Text>{person.cell}</Typography.Text>
                    </Row>
                  }
                />
              </Card>
            </Col>
          ))}
        </Row>
      </Row>
    </Row>
  );
}
