import React, { useEffect } from "react";
import UsersPresentational from "./users-presentational";
import { connect } from "react-redux";
import { fetchUsers, searchData, filteredData } from "../../../redux";

function UsersFunctional({ userData, fetchUsers, searchData, filteredData }) {
  useEffect(() => {
    fetchUsers();
  }, [fetchUsers]);

  useEffect(() => {
    filteredData(
      userData.users.filter((person) => {
        const firstName = person.name.first.toLowerCase();
        const lastName = person.name.last.toLowerCase();
        const fullName = firstName + " " + lastName;
        return fullName.includes(userData.search.toLowerCase());
      })
    );
  }, [filteredData, userData.search, userData.users]);
  console.log(userData.filteredUsers);
  return (
    <div>
      <UsersPresentational
        isLoading={userData.loading}
        persons={userData.filteredUsers}
        searchData={(e) => searchData(e.target.value)}
        isError={userData.error}
      />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    userData: state.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchUsers: () => dispatch(fetchUsers()),
    searchData: (search) => dispatch(searchData(search)),
    filteredData: (filteredPerson) => dispatch(filteredData(filteredPerson)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersFunctional);
