import React from "react";

import AddNewBankDetailsFunctional from "./components/add_new_bank_details-functional";
import "./add_new_bank_details.scss";

export default function AddNewBankDetails() {
  return <AddNewBankDetailsFunctional />;
}
