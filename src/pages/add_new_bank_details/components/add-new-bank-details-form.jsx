import React from "react";
import { Col, Row, Typography, Form, Input, Radio, Button } from "antd";

export default function AddNewBankDetailsForm() {
  return (
    <Col>
      <Row>
        <Col>
          <Typography.Title level={3}>Add New Bank Details</Typography.Title>
        </Col>
      </Row>
      <Col>
        <Form scrollToFirstError>
          <Col>
            <Form.Item
              name="bankname"
              label="Bank Name"
              rules={[
                {
                  required: true,
                  message: "Please enter the Bank Name!",
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col>
            <Form.Item
              name="accnickname"
              label="Acc Nick Name"
              rules={[
                {
                  required: true,
                  message: "Please enter the Account Nick Name!",
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col>
            <Form.Item
              name="routingno"
              label="Routing No"
              rules={[
                {
                  required: true,
                  message: "Please enter the Rounter No!",
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col>
            <Form.Item
              name="confirmroutingno"
              label="Confirm Routing No"
              rules={[
                {
                  required: true,
                  message: "Please re-enter the Rounter No!",
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col>
            <Form.Item
              name="Account Number"
              label="Account Number"
              rules={[
                {
                  required: true,
                  message: "Please enter the Account Number",
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col>
            <Form.Item
              name="confirmaccountno"
              label="Confirm Account Number"
              rules={[
                {
                  required: true,
                  message: "Please re-enter the Account Number",
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col>
            <Form.Item
              name="accounttype"
              label="Account Type"
              rules={[
                {
                  required: true,
                  message: "Please choose the Account Type",
                },
              ]}
              style={{ flexDirection: "row" }}
            >
              <Radio.Group name="radiogroup" defaultValue={1}>
                <Radio value={1}>Checking</Radio>
                <Radio value={2}>Savings</Radio>
              </Radio.Group>
            </Form.Item>
          </Col>
          <Row>
            <Col xl={12}>
              <Form.Item name="cancel">
                <Button htmlType="reset" className="cancel-btn">
                  Cancel
                </Button>
              </Form.Item>
            </Col>
            <Col xl={12}>
              <Form.Item name="addbank">
                <Button type="primary" htmlType="submit" className="add-btn">
                  Add Bank
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Col>
    </Col>
  );
}
