import React from "react";

import AddNewBankDetailsPresentational from "./add_new_bank_details-presentational";

export default function AddNewBankDetailsFunctional() {
  return <AddNewBankDetailsPresentational />;
}
