import React from "react";
import "./pending-request.scss";
import PendingRequestFunctional from "./components/pending-request-functional";
export default function PendingRequest() {
  return <PendingRequestFunctional />;
}
