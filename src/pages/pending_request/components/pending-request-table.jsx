import React from "react";
import { Table } from "antd";

export default function PendingRequestTable({ columns, data }) {
  return (
    <Table
      columns={columns}
      pagination={{ position: "bottomCenter" }}
      dataSource={data}
      xl
    />
  );
}
