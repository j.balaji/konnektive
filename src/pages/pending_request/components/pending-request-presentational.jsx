import React from "react";
import { Layout, Menu, Col, Row, Button, Typography, Select } from "antd";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";

import { images } from "./../../../assets/images/index";
import Avatar from "antd/lib/avatar/avatar";
import PendingRequestTable from "./pending-request-table";

const { Sider, Content, Footer } = Layout;
const { Option } = Select;

export default function PendingRequestPresentational({
  collapsed,
  toggle,
  data,
  columns,
}) {
  return (
    <Layout className="pending_request_layout">
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <Row className="logo">
          <img src={images.konnektivelogo} alt="logo" width="150px" />
        </Row>
        <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
          <Menu.Item key="1">
            <img src={images.dashboardicon} alt="dashboard" /> Dashboard
          </Menu.Item>
          <Menu.Item key="2">
            <img src={images.profileicon} alt="dashboard" /> Profile
          </Menu.Item>
          <Menu.Item key="3">
            <img src={images.affiliatemanagement} alt="pendingrequest" />{" "}
            Pending Requests
          </Menu.Item>
        </Menu>
        <Col style={{ paddingTop: "245px" }}>
          <Col style={{ paddingLeft: "5px" }}>
            <img
              src={images.logout}
              alt="logout"
              style={{ width: "190px", height: "225px" }}
            />
          </Col>
          <Col style={{ paddingLeft: "4px" }}>
            <Button
              style={{
                background: "midnightblue",
                border: "midnightblue",
                color: "white",
                width: "190px",
                height: "47px",
                borderRadius: "8px",
              }}
            >
              Log Out
            </Button>
          </Col>
          <Col>
            <Typography.Text
              style={{
                fontFamily: "ui-monospace",
                color: "white",
                letterSpacing: "1.5px",
                paddingLeft: "55px",
              }}
            >
              Version 0.1
            </Typography.Text>
          </Col>
        </Col>
      </Sider>
      <Layout className="site-layout">
        <Content style={{ margin: "0 16px" }}>
          <Row>
            <Col span={1}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: toggle,
                }
              )}
            </Col>
            <Col span={18}>
              <Typography.Title level={2} style={{ paddingTop: "8px" }}>
                Pending Requests
              </Typography.Title>
            </Col>
            <Col span={4.5} offset={0.5} className="profile">
              <Avatar src={images.prof3} /> Michael Jonnas
            </Col>
          </Row>
          <Row
            className="site-layout-background"
            style={{ paddingTop: "10px", paddingLeft: "10px" }}
          >
            <Col span={12}>
              <Typography.Title level={3}>Pending Requests</Typography.Title>
            </Col>
            <Col span={8}>
              <Row>
                <Col span={12}>
                  <Select
                    size="large"
                    defaultValue="Filter"
                    style={{ width: 200 }}
                  >
                    <Option key={1}>Ascending</Option>
                    <Option key={2}>Descending</Option>
                  </Select>
                </Col>
                <Col span={12}>
                  <Select
                    size="large"
                    defaultValue="Sortby"
                    style={{ width: 200 }}
                  >
                    <Option key={1}>Ascending</Option>
                    <Option key={2}>Descending</Option>
                  </Select>
                </Col>
              </Row>
            </Col>
          </Row>
          <PendingRequestTable data={data} columns={columns} />
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Ant Design ©2018 Created by Ant UED
        </Footer>
      </Layout>
    </Layout>
  );
}
