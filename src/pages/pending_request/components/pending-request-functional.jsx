import React, { useState } from "react";
import PendingRequestPresentational from "./pending-request-presentational";
import { Space, Button, Typography, Avatar } from "antd";
const { Link } = Typography;

export default function PendingRequestFunctional() {
  const columns = [
    {
      title: "FIRST NAME",
      dataIndex: "firstname",
      key: "firstname",
      render: (text) => (
        <>
          <Avatar
            style={{
              color: "#f56a00",
              backgroundColor: "#fde3cf",
              marginRight: "5%",
            }}
          >
            {text.charAt(0)}
          </Avatar>
          {text}
        </>
      ),
    },
    {
      title: "LAST NAME",
      dataIndex: "lastname",
      key: "lastname",
    },
    {
      title: "EMAIL ID",
      dataIndex: "emailid",
      key: "emailid",
    },
    {
      title: "PHONE NUMBER",
      dataIndex: "phonenumber",
      key: "phonenumber",
    },
    {
      title: "BUSINESS",
      dataIndex: "business",
      key: "business",
    },
    {
      title: "WEBSITE INFO",
      dataIndex: "websiteinfo",
      key: "websiteinfo",
      render: (text) => <Link>{text}</Link>,
    },
    {
      title: "",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <Button
            style={{
              background: "mediumseagreen",
              border: "mediumseagreen",
              color: "white",
              width: "80px",
              height: "25px",
              borderRadius: "4px",
            }}
          >
            Approve
          </Button>
          <Button
            style={{
              backgroundColor: "crimson",
              border: "crimson",
              color: "white",
              width: "80px",
              height: "25px",
              borderRadius: "4px",
            }}
          >
            Reject
          </Button>
        </Space>
      ),
    },
  ];

  const data = [
    {
      key: "1",
      firstname: "Balaji",
      lastname: "Janarthanan",
      emailid: "abc@gmail.com",
      phonenumber: "+02 90342304294",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "2",
      firstname: "Tony",
      lastname: "Stark",
      emailid: "abc@gmail.com",
      phonenumber: "+1 2342342124",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "3",
      firstname: "Black",
      lastname: "Widow",
      emailid: "black@gmail.com",
      phonenumber: "+99 96543048219",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "4",
      firstname: "Manuel",
      lastname: "Stark",
      emailid: "avn@gmail.com",
      phonenumber: "+43 4354351232",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "5",
      firstname: "Bruce",
      lastname: "Banner",
      emailid: "bruce@gmail.com",
      phonenumber: "+21 4923042123",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "6",
      firstname: "Nick",
      lastname: "Fury",
      emailid: "nick@gmail.com",
      phonenumber: "+91 98423421235",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "7",
      firstname: "Manuel",
      lastname: "Christoper",
      emailid: "amcn@gmail.com",
      phonenumber: "+1 42342313242",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "8",
      firstname: "Saleem",
      lastname: "Mohammad",
      emailid: "sm@gmail.com",
      phonenumber: "+32 14354234242",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "9",
      firstname: "Uday",
      lastname: "Stark",
      emailid: "uday@gmail.com",
      phonenumber: "+21 8972324123",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "10",
      firstname: "Sensit",
      lastname: "Stark",
      emailid: "sensit@gmail.com",
      phonenumber: "+21 4362721231",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "11",
      firstname: "Balaji",
      lastname: "Janarthanan",
      emailid: "abc@gmail.com",
      phonenumber: "+02 90342304294",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "12",
      firstname: "Tony",
      lastname: "Stark",
      emailid: "abc@gmail.com",
      phonenumber: "+1 2342342124",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "13",
      firstname: "Nick",
      lastname: "Fury",
      emailid: "nick@gmail.com",
      phonenumber: "+99 96543048219",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "14",
      firstname: "Manuel",
      lastname: "Stark",
      emailid: "avn@gmail.com",
      phonenumber: "+43 4354351232",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "15",
      firstname: "Bruce",
      lastname: "Banner",
      emailid: "bruce@gmail.com",
      phonenumber: "+21 4923042123",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "16",
      firstname: "Black",
      lastname: "Widow",
      emailid: "bw@gmail.com",
      phonenumber: "+91 98423421235",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "17",
      firstname: "Manuel",
      lastname: "Christoper",
      emailid: "amcn@gmail.com",
      phonenumber: "+1 42342313242",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "18",
      firstname: "Saleem",
      lastname: "Mohammad",
      emailid: "sm@gmail.com",
      phonenumber: "+32 14354234242",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "19",
      firstname: "Uday",
      lastname: "Stark",
      emailid: "uday@gmail.com",
      phonenumber: "+21 8972324123",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "20",
      firstname: "Sensit",
      lastname: "Maxwell",
      emailid: "sensit@gmail.com",
      phonenumber: "+21 4362721231",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
    {
      key: "21",
      firstname: "Saleem",
      lastname: "Mohammad",
      emailid: "sm@gmail.com",
      phonenumber: "+32 14354234242",
      business: "Affliaterocks",
      websiteinfo: "https://affliaterocks.com",
    },
  ];
  const [collapsed, setCollapsed] = useState(false);

  const toggle = () => {
    setCollapsed(!collapsed);
  };
  return (
    <PendingRequestPresentational
      columns={columns}
      data={data}
      collapsed={collapsed}
      toggle={toggle}
    />
  );
}
