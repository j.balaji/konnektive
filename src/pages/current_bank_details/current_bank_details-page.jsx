import React from "react";

import CurrentBankDetailsFunctional from "./components/current_bank_details-functional";
import "./current_bank_details.scss";

export default function CurrentBankDetails() {
  return <CurrentBankDetailsFunctional />;
}
