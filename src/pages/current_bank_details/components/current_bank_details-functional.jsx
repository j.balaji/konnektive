import React, { useState } from "react";
import CurrentBankDetailsPresentational from "./current_bank_details-presentational";

export default function CurrentBankDetailsFunctional() {
  const accounts = [
    {
      bankName: "HDFC BANK",
      accNickName: "BALAJI",
      routingNumber: 1123123232423423,
      accNumber: 1232432324342131,
      accType: "Savings",
    },
    {
      bankName: "SBI BANK",
      accNickName: "RAGURAM",
      routingNumber: 4555678996512545,
      accNumber: 5457812545465464,
      accType: "Savings",
    },
    {
      bankName: "HDFC BANK",
      accNickName: "BALAJI",
      routingNumber: 1123123232423423,
      accNumber: 1232432324342131,
      accType: "Savings",
    },
    {
      bankName: "SBI BANK",
      accNickName: "RAGURAM",
      routingNumber: 4555678996512545,
      accNumber: 5457812545465464,
      accType: "Savings",
    },
    {
      bankName: "HDFC BANK",
      accNickName: "BALAJI",
      routingNumber: 1123123232423423,
      accNumber: 1232432324342131,
      accType: "Savings",
    },
  ];

  const [collapsed, setCollapsed] = useState(false);

  const toggle = () => {
    setCollapsed(!collapsed);
  };
  return (
    <CurrentBankDetailsPresentational
      accounts={accounts}
      toggle={toggle}
      collapsed={collapsed}
    />
  );
}
