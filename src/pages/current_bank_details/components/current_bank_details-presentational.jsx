import React from "react";
import {
  Layout,
  Menu,
  Breadcrumb,
  Col,
  Row,
  Typography,
  Collapse,
  Button,
} from "antd";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";

import { images } from "../../../assets/images/index";
import Avatar from "antd/lib/avatar/avatar";
import CurrentBankDetailsForm from "./current-bank-details-form";

const { Header, Sider, Content, Footer } = Layout;
const { Panel } = Collapse;

export default function CurrentBankDetailsPresentational({
  accounts,
  collapsed,
  toggle,
}) {
  return (
    <Layout className="current_bank_details_layout">
      <Sider trigger={null} collapsible collapsed={collapsed}>
        <Row className="logo">
          <img src={images.konnektivelogo} alt="logo" width="150px" />
        </Row>
        <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
          <Menu.Item key="1">
            <img src={images.dashboardicon} alt="dashboard" /> Dashboard
          </Menu.Item>
          <Menu.Item key="2">
            <img src={images.profileicon} alt="dashboard" /> Profile
          </Menu.Item>
        </Menu>
        <Col style={{ paddingTop: "280px" }}>
          <Col style={{ paddingLeft: "5px" }}>
            <img
              src={images.logout}
              alt="logout"
              style={{ width: "190px", height: "225px" }}
            />
          </Col>
          <Col style={{ paddingLeft: "4px" }}>
            <Button
              style={{
                background: "midnightblue",
                border: "midnightblue",
                color: "white",
                width: "190px",
                height: "47px",
                borderRadius: "8px",
              }}
            >
              Log Out
            </Button>
          </Col>
          <Col>
            <Typography.Text
              style={{
                fontFamily: "ui-monospace",
                color: "white",
                letterSpacing: "1.5px",
                paddingLeft: "55px",
              }}
            >
              Version 0.1
            </Typography.Text>
          </Col>
        </Col>
      </Sider>
      <Layout className="site-layout">
        <Header className="site-layout-background" style={{ padding: 0 }} />
        <Content style={{ margin: "0 16px" }}>
          <Row>
            <Col span={1}>
              {React.createElement(
                collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                {
                  className: "trigger",
                  onClick: toggle,
                }
              )}
            </Col>
            <Col span={18}>
              <Breadcrumb style={{ margin: "16px 0" }}>
                <Breadcrumb.Item>Dashboard</Breadcrumb.Item>
                <Breadcrumb.Item>Bank Account Information</Breadcrumb.Item>
              </Breadcrumb>
            </Col>

            <Col span={4.5} offset={0.5} className="profile">
              <Avatar src={images.prof3} /> Michael Jonnas
            </Col>
          </Row>
          <Row
            className="site-layout-background"
            style={{ padding: 24, minHeight: "75vh" }}
          >
            <Col xl={12}>
              <CurrentBankDetailsForm accounts={accounts} />
            </Col>
            <Col xl={12} style={{ paddingLeft: "2%" }}>
              <Row>
                <Col>
                  <Typography.Title level={3}>
                    Inactive Accounts
                  </Typography.Title>
                </Col>
              </Row>
              <Row>
                <Col xl={24}>
                  <Collapse accordion>
                    {accounts.map((bank, key) => {
                      return (
                        <Panel
                          header={`${bank.bankName} ${bank.accNumber}`}
                          key={key}
                        >
                          <p>Bank Name: {bank.bankName}</p>
                          <p>Acc. Nick Name: {bank.accNickName}</p>
                          <p>Routing Number: {bank.routingNumber}</p>
                          <p>Acc. Number: {bank.accNumber}</p>
                          <p>Acc. Type: {bank.accType}</p>
                        </Panel>
                      );
                    })}
                  </Collapse>
                </Col>
              </Row>
            </Col>
          </Row>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          Ant Design ©2018 Created by Ant UED
        </Footer>
      </Layout>
    </Layout>
  );
}
