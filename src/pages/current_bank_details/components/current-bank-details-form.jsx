import React from "react";
import { Col, Row, Form, Input, Button, Typography, Select } from "antd";
import { PlusCircleFilled } from "@ant-design/icons";
import { images } from "../../../assets/images/index";
import { Link } from "react-router-dom";

const { Option } = Select;

export default function CurrentBankDetailsForm({ accounts }) {
  return (
    <Col className="currentbankdetails">
      <Row className="currentbankdetails_header">
        <Col xl={8} className="profile_image">
          <img src={images.prof3} alt="profile" />
        </Col>
        <Col xl={16}>
          <Typography.Title level={3}>Current Bank Details</Typography.Title>
          <Typography.Title level={4}>Checking</Typography.Title>
          <Typography.Title level={3}>{accounts[1].accNumber}</Typography.Title>
        </Col>
      </Row>

      <Row>
        <Col>
          <Link to="/add/new/bankdetails">
            {" "}
            <PlusCircleFilled /> Add New Bank Account{" "}
          </Link>
        </Col>
      </Row>
      <Col>
        <Form className="currentbankdetails_form" scrollToFirstError>
          <Col>
            <Form.Item
              className="currentbankdetails_form_item"
              name="bankname"
              label="Bank Name"
            >
              <Input defaultValue={accounts[1].bankName} />
            </Form.Item>
          </Col>
          <Col>
            <Form.Item
              className="currentbankdetails_form_item"
              name="accnickname"
              label="Acc Nick Name"
            >
              <Input defaultValue={accounts[1].accNickName} />
            </Form.Item>
          </Col>
          <Col>
            <Form.Item
              className="currentbankdetails_form_item"
              name="routingno"
              label="Routing No"
            >
              <Input defaultValue={accounts[1].routingNumber} />
            </Form.Item>
          </Col>
          <Col>
            <Form.Item
              className="currentbankdetails_form_item"
              name="Account Number"
              label="Account Number"
            >
              <Input defaultValue={accounts[1].accNumber} />
            </Form.Item>
          </Col>
          <Col>
            <Form.Item
              className="currentbankdetails_form_item"
              name="accounttype"
              label="Account Type"
            >
              <Select
                labelInValue
                defaultValue={{ value: accounts[1].accType }}
                style={{ width: 120 }}
              >
                <Option value="checking">checking</Option>
                <Option value="saving">saving</Option>
              </Select>
            </Form.Item>
          </Col>
          <Row>
            <Col xl={12}>
              <Form.Item name="cancel">
                <Button htmlType="reset" className="cancel-btn">
                  Cancel
                </Button>
              </Form.Item>
            </Col>
            <Col xl={12}>
              <Form.Item name="updatebank">
                <Button type="primary" htmlType="submit" className="add-btn">
                  Update Bank
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Col>
    </Col>
  );
}
