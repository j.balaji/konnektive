import axios from "axios";
import {
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE,
  SEARCH_DATA,
  FILTERED_DATA,
} from "./userTypes";

export const fetchUsers = () => {
  return (dispatch) => {
    dispatch(fetchUsersRequest());
    axios
      .get("https://randomuser.me/api/?results=20")
      .then((response) => {
        // response.data is the users
        console.log(response.data.results);
        const users = response.data.results;
        dispatch(fetchUsersSuccess(users));
      })
      .catch((error) => {
        // error.message is the error message
        dispatch(fetchUsersFailure(error.message));
      });
  };
};

export const fetchUsersRequest = () => {
  return {
    type: FETCH_USERS_REQUEST,
  };
};

export const fetchUsersSuccess = (users) => {
  return {
    type: FETCH_USERS_SUCCESS,
    payload: users,
  };
};

export const fetchUsersFailure = (error) => {
  return {
    type: FETCH_USERS_FAILURE,
    payload: error,
  };
};

export const searchData = (search) => {
  return {
    type: SEARCH_DATA,
    payload: search,
  };
};

export const filteredData = (filteredPerson) => {
  return {
    type: FILTERED_DATA,
    payload: filteredPerson,
  };
};
