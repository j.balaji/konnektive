import {
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE,
  SEARCH_DATA,
  FILTERED_DATA,
} from "./userTypes";

const initialState = {
  loading: false,
  users: [],
  error: "",
  search: "",
  filteredUsers: [],
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USERS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_USERS_SUCCESS:
      return {
        ...state,
        loading: false,
        users: action.payload,
        error: "",
      };
    case FETCH_USERS_FAILURE:
      return {
        ...state,
        loading: false,
        users: [],
        error: action.payload,
      };
    case SEARCH_DATA:
      return {
        ...state,
        search: action.payload,
      };
    case FILTERED_DATA:
      return {
        ...state,
        filteredUsers: state.users.filter((person) => {
          const firstName = person.name.first.toLowerCase();
          const lastName = person.name.last.toLowerCase();
          const fullName = firstName + " " + lastName;
          const search = "" + state.search;
          console.log(search);
          return fullName.includes(search.toLowerCase());
        }),
      };
    default:
      return state;
  }
};

export default userReducer;
